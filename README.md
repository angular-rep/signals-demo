# signals-demo

## Getting started

## My Project

[Link to medium profile](https://medium.com/@rkanagasikamani)

## Description

This is a simple project that I created to learn about angular signals.

## How to use

To use this project, simply visit the demo link above.

## Credits

This project was created by me.

## License

This project is distributed under the MIT License.
