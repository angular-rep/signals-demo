import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalExampleComponent } from './signal-example/signal-example.component';
import { WritableSignalExampleComponent } from './writable-signal-example/writable-signal-example.component';
import { ComputedSignalExampleComponent } from './computed-signal-example/computed-signal-example.component';
import { EffectSginalExampleComponent } from './effect-sginal-example/effect-sginal-example.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: SignalExampleComponent,
  },
  {
    path: 'writable',
    component: WritableSignalExampleComponent,
  },
  {
    path: 'computed',
    component: ComputedSignalExampleComponent,
  },
  {
    path: 'effect',
    component: EffectSginalExampleComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
