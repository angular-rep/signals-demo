import { Injectable, Signal, effect } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map, tap } from 'rxjs';
import { toSignal } from '@angular/core/rxjs-interop';

@Injectable({
  providedIn: 'root'
})
export class SignalService {
  //instruments: Instrument[] = [];
  instrumentData$!: Observable<Instrument>;

  instrument!: Signal<Instrument | undefined>;

  constructor(private http: HttpClient) {
    this.instrumentData$ = this.http.get<Node>("http://localhost:3000/music/")
      .pipe(
        map((node: Node) => ({
          name: node.name,
          definition: node.definition || "",
          examples: node.children?.flatMap((node: Node) => node.name),
          ins: node.children?.map((child: Node) => {
            return {
              name: child.name,
              definition: child.definition,
              examples: child.children?.flatMap(node => node.name)
            }
          })
        } as Instrument)
        ),
        tap(() => console.log)
      );
    this.instrument = toSignal(this.instrumentData$, { initialValue: {} as Instrument });
    effect(() => {
      console.log("triggered : ->", this.instrument());
    })
  }



  


}

export interface Instrument {
  name?: string;
  definition?: string;
  examples?: string[]
  ins?: Instrument[];
}



export interface Node {
  name: string,
  definition?: string,
  children?: Node[];
}

