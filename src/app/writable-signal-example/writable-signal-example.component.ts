import { Component, signal } from '@angular/core';

@Component({
  selector: 'app-writable-signal-example',
  templateUrl: './writable-signal-example.component.html',
  styleUrls: ['./writable-signal-example.component.scss']
})
export class WritableSignalExampleComponent {

  writableSignal = signal<number>(0);

  increament() {
    this.writableSignal.update((val) => val + 1);
  }
}
