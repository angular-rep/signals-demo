import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WritableSignalExampleComponent } from './writable-signal-example.component';

describe('WritableSignalExampleComponent', () => {
  let component: WritableSignalExampleComponent;
  let fixture: ComponentFixture<WritableSignalExampleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WritableSignalExampleComponent]
    });
    fixture = TestBed.createComponent(WritableSignalExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
