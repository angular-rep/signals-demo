import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectSginalExampleComponent } from './effect-sginal-example.component';

describe('EffectSginalExampleComponent', () => {
  let component: EffectSginalExampleComponent;
  let fixture: ComponentFixture<EffectSginalExampleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EffectSginalExampleComponent]
    });
    fixture = TestBed.createComponent(EffectSginalExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
