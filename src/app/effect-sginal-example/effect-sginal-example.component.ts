import { Component, computed, signal, effect } from '@angular/core';

@Component({
  selector: 'app-effect-sginal-example',
  templateUrl: './effect-sginal-example.component.html',
  styleUrls: ['./effect-sginal-example.component.scss']
})
export class EffectSginalExampleComponent {
  writableSignal = signal<number>(0);
  Account_Name = "";
  computedSignal = computed(() => {
    return this.writableSignal() % 2 == 0 ? 'Even' : 'Odd';
  });


  increament() {
    this.writableSignal.update(val => val + 1);
  }

  constructor(){

    effect(()=>{
      console.log('effect -> countSignal = ', this.writableSignal(), "; computedSignal = ", this.computedSignal());
    })
  }

}
