import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WritableSignalExampleComponent } from './writable-signal-example/writable-signal-example.component';
import { ComputedSignalExampleComponent } from './computed-signal-example/computed-signal-example.component';
import { EffectSginalExampleComponent } from './effect-sginal-example/effect-sginal-example.component';

@NgModule({
  declarations: [
    AppComponent,
    WritableSignalExampleComponent,
    ComputedSignalExampleComponent,
    EffectSginalExampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
