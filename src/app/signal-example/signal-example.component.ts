/* eslint-disable @angular-eslint/template/click-events-have-key-events */
/* eslint-disable @angular-eslint/template/interactive-supports-focus */
import { Component, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Instrument, SignalService } from '../signal-service.service';

import { computed } from '@angular/core';

@Component({
  selector: 'app-signal-example',
  standalone: true,
  imports: [CommonModule],
  template: `

  <h1>{{instrument()?.name}}</h1>
  <h2>Definition: </h2>
  <p>{{instrument()?.definition}}</p>
  <div>{{instrument()?.examples}}
    <ol>
      <li (keyup)="onKeyup(item)" (click)="instrumentSelected(item)" *ngFor="let item of instrument()?.examples">
          {{item}}
        </li>
      </ol>
  </div>

  <h3>{{selected().definition}}</h3>

  <div>
  <ol>
      <li (click)="instrumentSelected(childItem)" *ngFor="let childItem of selected()?.examples">
          {{childItem}}
        </li>
      </ol>
  </div>


    `,
  styleUrls: ['./signal-example.component.scss']
})
export class SignalExampleComponent {

  instrument = computed(() => this.signalService.instrument());

  selected = signal<Instrument>({});

  constructor(private signalService: SignalService) { }

  instrumentSelected(item: string) {
    console.log(item, " selected!");
    this.selected.set(this.instrument()?.ins?.find(n => n.name === item) || {})

    console.log('selected instrument -> ', this.selected);
  }

  onKeyup(item: string) {
    this.instrumentSelected(item);
  }
}
