import { Component, computed, signal } from '@angular/core';

@Component({
  selector: 'app-computed-signal-example',
  templateUrl: './computed-signal-example.component.html',
  styleUrls: ['./computed-signal-example.component.scss'],
})
export class ComputedSignalExampleComponent {
  writableSignal = signal<number>(0);

  computedSignal = computed(() => {
    return this.writableSignal() % 2 == 0 ? 'Even' : 'Odd';
  });
  increament() {
    this.writableSignal.update(val => val + 1);
  }
}
